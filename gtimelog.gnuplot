#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 5.2 patchlevel 6    last modified 2019-01-01 
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2018
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')
# set terminal qt 0 font "Sans,9"
# set output

set title "Working time distribution"

set border 3
set grid xtics back
set rmargin 8

unset key
#set key noreverse enhanced autotitles box linetype -1 linewidth 1.000
#set key vert out bottom center
#set key width 2 height 1

set xdata time
set timefmt "%Y-%m-%d"
set format x "%d %b %y"
set format y "%02.0f:00"
set xtics out nomirror rotate by -45

set ylabel "working hours" rotate by 90
set ytics out nomirror 1
set yrange [*:24]

# Define a color palette
set palette maxcolors 4
set palette model RGB defined ( \
  0 '#5c3dcc', \
  1 '#43c200', \
  2 '#ef2929', \
  3 '#fbbc05')

# Define palette labels
set cbrange [-0.5:3.5]
set cbtics scale 0 ( \
  'Collabora'  0, \
  'R\&amp;D'   1, \
  'Magic Leap' 2, \
  'Google'     3)

# Define a function to map task names to color indices in the palette
task_to_color(task) = ( \
    task eq 'collabora:'  ? 0 : \
    task eq 'r&d:'        ? 1 : \
    task eq 'magic-leap:' ? 2 : \
    task eq 'google:'     ? 3 : \
    -1)

_w = 1280
_h = _w * 0.4

#set terminal qt 0 size _w,_h
set terminal svg background "#ffffff" butt enhanced font "arial,10" fontscale 1.0 size _w,_h
set output 'gtimelog_plot.svg'

p '< cat ~/.gtimelog/timelog.txt | sed -e "s/^.*\: .*\*\*.*$/\n\0/g" -e "s/\([0-9][0-9]\):\([0-9][0-9]\)/\1 \2/g"' \
  using 1:($2 + $3/60.):(task_to_color(stringcolumn(5))) w l lc palette lw 2.2 \
  t "Working time" \
#    EOF
